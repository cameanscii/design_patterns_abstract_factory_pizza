package pl.sda.pizza;

import java.util.HashSet;
import java.util.Set;

public class Main{
    public static void main(String[] args) {
        Set<Pizza> pizzas = new HashSet<>();

        pizzas.add(Pizzeria.stworzCzterySery());
        pizzas.add(Pizzeria.stworzHawajska());
//        pizzas.add(Pizzeria.stworzHawajska());
//        pizzas.add(Pizzeria.stworzHawajska());
        pizzas.add(Pizzeria.stworzMargarrite());
        pizzas.add(Pizzeria.stworzWiejska());

        System.out.println(pizzas);
        System.out.println(pizzas.size());

        System.out.println(Pizzeria.stworzHawajska().getSkladniki());
    }
}
