package pl.sda.pizza;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Pizza {
    private String nazwa;
    private List<String> skladniki = new ArrayList<String>();

    public Pizza(String nazwa) {
        this.nazwa = nazwa;
    }

    public Pizza(String nazwa, List<String> skladniki) {
        this.nazwa = nazwa;
        this.skladniki = skladniki;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public List<String> getSkladniki() {
        return skladniki;
    }

    public void setSkladniki(List<String> skladniki) {
        this.skladniki = skladniki;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "nazwa='" + nazwa + '\'' +
                ", skladniki=" + skladniki +
                '}';
    }

    public static class PizzaBuilder {
        private String nazwa;
        private List<String> skladniki = new ArrayList<String>();

        public PizzaBuilder() {
        }

        public PizzaBuilder setNazwa(String nazwa) {
            this.nazwa = nazwa;
            return this;
        }

        public PizzaBuilder dodajSkladnik(String skladnik) {
            this.skladniki.add(skladnik);
            return this;
        }

        public Pizza stworz() {
            return new Pizza(nazwa, skladniki);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pizza pizza = (Pizza) o;
        return Objects.equals(getNazwa(), pizza.getNazwa()) &&
                Objects.equals(getSkladniki(), pizza.getSkladniki())
                && getSkladniki().containsAll(pizza.getSkladniki())
                && pizza.getSkladniki().containsAll(getSkladniki())
                && getSkladniki().size() == pizza.getSkladniki().size();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getNazwa(), getSkladniki());
    }
}

